# airprint-generate

## <a name="about"></a> [About](#toc)
- image is always based on latest stable AlpineLinux v3
- automated builds - daily rebuild images
- multi-arch: amd64, arm32v6, arm32v7, arm64
- runs in the background and once the cups printer config changes: regenerate the airprint avahi configs
- monitors the cups config and re-creates the airprint config on changes
- combines enhancements and fixes from many other forks of airprint-generate

Forked and modified copy of source code at:
- https://github.com/tjfontaine/airprint-generate

# <a name="intro"></a> [Intro](#toc)
This script will generate avahi .service files for shared CUPS printers.

This script will connect to a CUPS server and for each printer configured and
marked as shared will generate a .service file for avahi that is compatible
with Apple's AirPrint announcements. Any printer that can be configured to work
with CUPS can be used. Printers should not be configured in CUPS as raw, unless
the printer can natively print PDF. That is to say, CUPS needs to already be
configured with a PDF filter. Debian based distributions ship CUPS pre-configured
this way.

DNSSD has a limit of 255 Chars for a given txt-record, because of this the list
of accepted pdl's will be truncated to fit. If you're curious to see which ones
are trimmed out of the list run with the script with the verbose flag (--verbose)

# <a name="multi-arch"></a> [Multi-arch Image](#toc)
The below commands reference a
[Docker Manifest List](https://docs.docker.com/engine/reference/commandline/manifest/)
at [`michaelbalser/airprint-generate`](https://hub.docker.com/repository/docker/michaelbalser/airprint-generate).

Simply running commands using this image will pull
the matching image architecture (e.g. `amd64`, `arm32v6`, `arm32v7`, or `arm64`) based on
the hosts architecture. Hence, if you are on a **Raspberry Pi** the below
commands will work the same as if you were on a traditional `amd64`
desktop/laptop computer.

### Usage: airprint-generate.py [options]

Options:
  -h, --help            show this help message and exit
  -H HOSTNAME, --host=HOSTNAME
                        Hostname of CUPS server (optional)
  -P PORT, --port=PORT  Port number of CUPS server
  -u USER, --user=USER  Username to authenticate with against CUPS
  -d DIRECTORY, --directory=DIRECTORY
                        Directory to create service files
  -v, --verbose         Print debugging information to STDERR
  -p PREFIX, --prefix=PREFIX
                        Prefix all files with this string

## Docker containerized avahi .service generation

After the printers have been configured in the cups server, docker can interactively generate the avahi .service
while making use of the container.

* Build the container

```shell
docker build -t airprint-generate .
```

* Generate the avahi service by defining the ip address of the cups server

```shell
docker create \
  --name airprint-generate \
  --restart always \
  --volume /etc/cups:/config \
  --volume /etc/avahi/services:/services \
  --env CUPS_HOST=<YOUR_CUPS_IP> \
  --env CUPS_PORT=631 \
  michaelbalser/airprint-generate:latest
```
