#!/bin/bash

inotifywait -m -e close_write,moved_to,create /config |
while read -r directory events filename; do
    if [ "$filename" = "printers.conf" ]; then
        rm -rf /services/AirPrint-*.service
        /airprint-generate.py --host "${CUPS_HOST}" --port "${CUPS_PORT}" -d /services
    fi
done
