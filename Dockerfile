# base image
ARG ARCH=amd64
FROM $ARCH/alpine:3

# args
ARG VCS_REF
ARG BUILD_DATE

ENV CUPS_HOST=127.0.0.1
ENV CUPS_PORT=631

# labels
LABEL maintainer="Michael Balser <michael@balser.cc>" \
  org.label-schema.schema-version="1.0" \
  org.label-schema.name="michaelbalser/airprint-generate" \
  org.label-schema.description="Generate avahi .service files for shared CUPS printers" \
  org.label-schema.version="0.1" \
  org.label-schema.url="https://hub.docker.com/repository/docker/michaelbalser/airprint-generate" \
  org.label-schema.vcs-url="https://gitlab.com/mbalser1/airprint-generate" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.build-date=$BUILD_DATE

# We want a mount for these
VOLUME /config
VOLUME /services

# install packages
RUN apk --no-cache --no-progress add \
    bash \
    cups-libs \
    inotify-tools \
    py3-libxml2 \
    py3-pip \
    py3-pycups \
    tini

COPY files ./
RUN chmod +x /printer-update.sh /airprint-generate.py

ENTRYPOINT ["/sbin/tini", "--"]
CMD ["/printer-update.sh"]
